import { Button, Dialog, DialogTitle, Typography, DialogContent, DialogActions } from '@material-ui/core';
import React from 'react';
const Dialogs = (props) => {
    const { open, handleClose, selectedArticle } = props
    return (
        <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
            <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                <b>{selectedArticle.title}</b>
            </DialogTitle>
            <DialogContent dividers>
                <Typography gutterBottom>
                    <img style={{ height: 300, width: "100%" }} src={selectedArticle.urlToImage} title="Contemplative Reptile" />
                </Typography>
                <Typography gutterBottom>
                    {selectedArticle.content}
                </Typography>
                <Typography gutterBottom>
                    <b>Author</b> :  {selectedArticle.author}
                </Typography>
                <Typography gutterBottom>
                    <b>Published At</b> : {selectedArticle.publishedAt}
                </Typography>
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleClose} color="primary">
                    Cancel
              </Button>
            </DialogActions>
        </Dialog>

    );

}

export default Dialogs;