import logo from './logo.svg';
import './App.css';
import Content from './content';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p> News</p>
      </header>
    <Content />
    </div>
  );
}

export default App;
