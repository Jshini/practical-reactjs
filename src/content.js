// import { Card, CardImg, CardBody, CardTitle, CardText, Row, Col, } from 'reactstrap'
import { Card, CardActionArea, CardContent, CardMedia, Container, Grid, Typography, Button } from '@material-ui/core';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Dialogs from './dialogs';

const Content = () => {
    const [articles, setArticles] = useState([])
    const [open, setOpen] = React.useState(false);
    const [selectedArticle, setSelectedArticle] = useState({})
    const [selectedTab, setSelectedTab] = useState(1)
    const [search, setSearch] = useState("bitcoin")

    // Defined styles for content
    const styles = {
        root: {
            maxWidth: 345,
        },
        media: {
            height: 140,
        },
        search_row: {
            height: '45px',
            padding: 5,
            textAlign: 'center'
        },
        search_input: {
            height: '30px',
            width: '60%',
            borderRadius: 8,
            borderWidth: 1,
            borderColor: "gray",
            fontSize: 17,
            padding:"4px 10px"
        },
        cards: {
            // backgroundColor: 'red'
        },
        row_data: {
            display: 'flex',
        },
        button:{
            margin: '5px',
        }
    }
    useEffect(()=>{
        // calling get article list api ( everything)
        getNewsListApi(search)
    },[1])

    const handleClickOpen = () => {
        setOpen(true);
      };
    const handleClose = () => {
        setOpen(false);
    };

    // get all the articles
    const getNewsListApi =(searchString)=>{
        setSelectedTab(1)
        axios.get(`https://newsapi.org/v2/everything?q=${searchString}&apiKey=6fa5626063ec4ba9acaeba421e0c390d`).then((res)=>{
        console.log("res", res)
        if(res.status === 200){
            setArticles(res.data.articles)
        }
        }).catch((err)=>{
            alert(err.response.message)
        })
    }

    // on Change search value
    const onChangeSearch = (event) => {
        setSearch(event.target.value)
        getNewsListApi()
    }


    const onCardPress=(event,item)=>{
        setSelectedArticle(item)
        handleClickOpen(event.target.value)
    }

    // on Press top headlines button
    const pressTopButton=()=>{
        setSelectedTab(2)
        axios.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=6fa5626063ec4ba9acaeba421e0c390d').then((res)=>{
            console.log("res", res)
            if(res.status === 200){
                setArticles(res.data.articles)
            }
            }).catch((err)=>{
                alert(err.response.message)
            })
    }

    return (
        <>
            <div style={styles.search_row}>
                <Button variant="contained" color={selectedTab == 1 ?"primary" : "default"} onClick={()=>getNewsListApi()} style={styles.button}>All</Button>
               <Button  variant="contained" color={selectedTab == 2 ?"primary" : "default"} onClick={()=>pressTopButton()}>Top HeadLines</Button>
            </div>
            <div style={styles.search_row}>
                <input name="search" placeholder="search" onChange={onChangeSearch} defaultValue={search} style={styles.search_input} />
            </div>
            <Container maxWidth="lg">
                <Grid container spacing={2}>
                {
                    articles.map((item,index)=>{
                        return(
                            <Grid item xs={3}>
                            <Card style={styles.root} onClick={(event)=>onCardPress(event,item)}>
                                <CardActionArea>
                                    <CardMedia
                                        style={styles.media}
                                        image={item.urlToImage}
                                        title="Contemplative Reptile"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                           {item.title}
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            {item.content}
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                        )
                    })
                }

                </Grid>
            </Container>

            {/* show - when user click on a card */}
            <Dialogs open={open} selectedArticle={selectedArticle} handleClose={handleClose}/>
         </>
    );
}

export default Content;